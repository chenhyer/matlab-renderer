OpenCV calibration code wrapper (mex file). Used with the renderer function available from:
http://www.openu.ac.il/home/hassner/code/renderer

This code was developed by Liav Assif and Tal Hassner, and was used in the following papers: 

T. Hassner, Viewing Real-World Faces in 3D, International Conference on Computer Vision (ICCV), Sydney, Austraila, Dec. 2013
T. Hassner, L. Assif, and L. Wolf, When Standard RANSAC is Not Enough: Cross-Media Visual Matching with Hypothesis Relevancy, Machine Vision and Applications (MVAP), to appear 

If you find this code useful, please add suitable references in your work to either (or both) of these papers.

Copyright 2013, Liav Assif and Tal Hassner

The SOFTWARE ("renderer" and \ or  "calib") is provided "as is", without any guarantee made as to its suitability or fitness for any particular use.  It may contain bugs, so use of this tool is at your own risk. We take no responsibility for any damage that may unintentionally be caused through its use.

Dependencies:
OpenCV
http://opencv.org/


Calibration code
----------------
The code depends on OpenCV (1.1):
When compiling using MATLAB 'mex', the library path to these libraries can be provided using -L, e.g. if they are stored locally in ../local then:
mex calib.cpp POSIT.cpp util/util.cpp -lcv -lcxcore -Iutil/ -L../local/lib -I../local/include
In order to be used from MATLAB the dependant libraries must be added to MATLAB's libary path. There are several ways this can be achieved. The simple way is to use the provided 'ldlibMatlab' BASH script to run MATLAB. Modify LIBS_PREFIX to the appropriate locations. Other ways are possible and can be found in MATLAB's documentation.

See doCalib.m for an example:
[A,R,T]=doCalib(width,height,imPoints,objPoints,A,RGuess,TGuess)

RGuess and TGuess are optional. A must be provided. See the documentation at
http://docs.opencv.org/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
for more information.
